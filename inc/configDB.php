<?php
/**
* Code from: https://github.com/rorystandley/MySQL-CRUD-PHP-OOP/blob/master/class/mysql_crud.php
*/
class Database
{
	private $db_host = "localhost";
	private $db_user = "id302501_test";
	private $db_pass = "testing";
	private $db_name = "id302501_remytest";
	private $con = false;
	private $result = array();
	private $myQuery = "";
	private $numResults = "";
	public $errorType = "";
	private $staff = array("1" => array(
		"FirstName" => "Oliver",
		"LastName"	=> "Smith",
		"Address"	=>	"119 Boulevard Robert Schuman, 93190 Livry-Gargan, France", 
		"email"	=>	"kunleoladimeji@gmail.com",
		"latitude"	=>	48.9321953, //Latitude and longitude not really needed as calculation done by address
		"longitude"	=>	2.5577283
		), 
		"2" => array(
			"FirstName"	=> "Rajeev",
			"LastName"	=>	"Kistoo",
			"Address"	=>	"110 Rue de Meaux, 93410 Vaujours, France",
			"email"	=> "rajeev.kistoo@test.mu",
			"latitude"	=>	48.9348871,
			"longitude"	=>	2.5718629
		),
		"3"	=> array(
			"FirstName"	=> "Olakunle",
			"LastName"	=>	"Oladimeji",
			"Address"	=>	"145 Avenue Voltaire, 93190 Livry-Gargan, France",
			"email"	=>	"olakunle.oladimeji@test.mu",
			"latitude"	=> 48.9328538,
			"longitude"	=>	2.556342
		),
		"4"	=> array(
			"FirstName"	=> "Tee",
			"LastName"	=>	"Famakin",
			"Address"	=>	"8 Avenue du Stade de France, 93210 Saint-Denis, France",
			"email"	=>	"tee.famakin@test.mu",
			"latitude"	=>	48.9215875,
			"longitude"	=>	2.3622495
		),
		"5"	=> array(
			"FirstName"	=> "Remy",
			"LastName"	=>	"Grandpierre",
			"Address"	=>	"3 Rue de la Cokerie, 93210 Saint-Denis, France",
			"email"	=>	"remy.grandpierre@test.mu",
			"latitude"	=>	48.9211961,
			"longitude"	=>	2.3629252
		)
	);
	function __construct()
	{
		$this->connect();
		$this->setupTables();
	}

	//Could Return as array, but test says from database, so i use returnAddressDB()
	public function returnAddress($staff)
	{
		return json_encode($this->staff);
	}

	public function returnAddressDB()
	{
		$count = 1;
		$this->myQuery = "Select * from staff";
		$returnedArray = array();
		$result = mysql_query($this->myQuery);
		while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
			$returnedArray[$count] = array('FirstName' => $row['FirstName'], 'LastName' => $row['LastName'], 'email' => $row['email'], 'Address' => $row['Address'], 'longitude' => $row['longitude'], 'latitude' => $row['latitude']);
			$count++;
		}
		return json_encode($returnedArray);
	}

	//Once again, could return from array, but DB is preferrable, so using return returnStaffDetailsDB()
	public function returnStaffDetails($staffID)
	{
		$staffdetails = array();
		$staffdetails["FullName"] = $this->staff[$staffID+1]["FirstName"] . " " . $this->staff[$staffID+1]["LastName"];
		$staffdetails["Email"] = $this->staff[$staffID+1]["email"];
		return $staffdetails;
	}

	public function returnStaffDetailsDB($staffID)
	{
		$this->myQuery = "Select * from staff WHERE id=" .$staffID;
		$returnedArray = array();
		$result = mysql_query($this->myQuery);
		while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
			$returnedArray["id"] = $row["id"];
			$returnedArray["FullName"] = $row["FirstName"] . " " .$row["LastName"];
			$returnedArray["Email"] = $row["email"];
		}
		return $returnedArray;
	}

	public function updateLeadCount($staffID)
	{
		$this->myQuery = 'UPDATE staff SET leads = leads + 1 WHERE id='.$staffID;
		if ($result = mysql_query($this->myQuery)) {
			return true;
		}
		else {
			return false;
		}
	}

	public function connect() {
		if (!$this->con) {
			$myconn = @mysql_connect($this->db_host, $this->db_user, $this->db_pass);
			if ($myconn) {
				$seldb = @mysql_select_db($this->db_name, $myconn);
				if ($seldb) {
					$this->con = true;
					return true;
				}
				else {
					array_push($this->result, mysql_error());
					return false;
				}
			}
			else {
				array_push($this->result, mysql_error());
				return false;
			}
		}
		else {
			return true;
		}
	}

	public function disconnect() {
		if (!$this->con) {
			if (@mysql_close()) {
				$this->con = false;
				return true;
			}
			else {
				return false;
			}
		}
	}

	public function insert($table, $params=array())
	{
		if ($this->tableExists($table)) {
			$sql = 'INSERT INTO `' .$table.'` (`'.implode('`, `', array_keys($params)).'`) VALUES ("' . implode('", "', $params). '")';
			$this->myQuery= $sql;
			if ($ins = @mysql_query($sql)) {
				array_push($this->result, mysql_insert_id());
				return true;
			}
			else {
				array_push($this->result, mysql_error());
				return false;
			}
		} else {
			return false;
		}
	}
	public function sql($sql) {
		$query = @mysql_query($sql);
		$this->myQuery = $sql;
		if ($query) {
			$this->numResults = mysql_num_rows($query);
			for ($i=0; $i < $this->numResults; $i++) { 
				$r = mysql_fetch_array($query);
				$key = array_keys($r);
				for ($x=0; $x < count($key); $x++) { 
					if (!is_int($key[$x])) {
						if (mysql_num_rows($query) >= 1) {
							$this->result[$i][$key[$x]] = $r[$key[$x]];
						}
						else {
							$this->result = null;
						}
					}
				}
			}
			return true;
		}
		else {
			array_push($this->result, mysql_error());
			return false;
		}
	}

	private function tableExists($table){
		$tablesInDb = @mysql_query('SHOW TABLES FROM ' .$this->db_name. ' LIKE "' .$table. '"');
		if ($tablesInDb) {
			if (mysql_num_rows($tablesInDb)==1) {
				return true;
			}
			else {
				array_push($this->result, $table." does not exist in this database");
				return false;
			}
		}
	}

	public function setupTables() {
		if (!$this->tableExists("contact")) {
			//Dont understand why foreign key isnt working on staff_assigned_id - To check!
			$contactSQL = "CREATE TABLE contact (id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, fullname VARCHAR(50) NOT NULL, address VARCHAR(100) NOT NULL, longitude DOUBLE NOT NULL, latitude DOUBLE NOT NULL, email VARCHAR(50) NOT NULL, phone INT(11) NOT NULL, message VARCHAR(150) NOT NULL, staff_assigned_id INT(6), message_date TIMESTAMP)";
			$this->sql($contactSQL);
		}
		if (!$this->tableExists("staff")) {
			$staffSQL = "CREATE TABLE staff (id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, FirstName VARCHAR(50) NOT NULL, LastName VARCHAR(50) NOT NULL, Address VARCHAR(100) NOT NULL, email VARCHAR(50) NOT NULL, latitude DOUBLE NOT NULL, longitude DOUBLE NOT NULL, leads INT(6) DEFAULT 0)";
			$count = count($this->staff);
			if ($this->sql($staffSQL)) {
				for ($i=1; $i <= $count; $i++) { 
					$staffData = $this->staff[$i];
					$this->insert("staff", $staffData); 
				}
			}
		}
	}
	public function getResult() {
		$val = $this->result;
		$this->result = array();
		return $val;
	}

	public function getSql() {
		$val = $this->myQuery;
		$this->myQuery = array();
		return $val;
	}

	public function numRows() {
		$val = $this->numResults;
		$this->numResults = array();
		return $val;
	}
	public function escapeString($data) {
		return mysql_real_escape_string($data);
	}
}


?>