
<?php
/**
 * This example shows sending a message using a local sendmail binary.
 */
require 'mailer/PHPMailerAutoload.php';
//Create a new PHPMailer instance
$mail = new PHPMailer;
// Set PHPMailer to use the sendmail transport
$mail->isSendmail();
//Set who the message is to be sent from
$mail->setFrom('test@jobsite.mu', 'Mailer');
//Set an alternative reply-to address
$mail->addReplyTo('test@jobsite.mu', 'Mailer');
//Set who the message is to be sent to
$mail->addAddress('kunleoladimeji@gmail.com', 'Olakunle Oladimeji');
//Set the subject line
$mail->Subject = 'PHPMailer sendmail test';
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
$mail->Body = 'This is a test email';
//Replace the plain text body with one created manually
$mail->AltBody = 'This is a plain-text message body';
//send the message, check for errors
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    echo "Message sent!";
}
