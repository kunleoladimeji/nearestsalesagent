<?php
include 'configDB.php';
require 'mailer/PHPMailerAutoload.php';
$mydb = new Database();
$mydb->setupTables();
$mail = new PHPMailer;
$mail->isSendmail();
$mail->setFrom('admin@remytest.000webhostapp.com', 'Contact Form Mailer'); //Add from email here
$mail->addReplyTo('admin@remytest.000webhostapp.com', 'Contact Form Mailer'); //Add reply to email here
$mail->isHTML(true); 

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	$action = $_GET["action"];
	switch ($action) {
		case 'nearestStaff':
			$addresses = $mydb->returnAddressDB();
			echo($addresses);
			break;
		case 'submitForm':
			//$staff = $mydb->returnStaffDetails($_GET["staff"]);
			$staff = $mydb->returnStaffDetailsDB($_GET['staff']);
			$query = array('fullname' => $_GET['fullname'], 'address' => $mydb->escapeString($_GET['address']), 'longitude' => $_GET['longitude'], 'latitude' => $_GET['latitude'], 'email' => $mydb->escapeString($_GET['email']), 'phone' => $_GET['phone'], 'message' => $_GET['message'], 'staff_assigned_id' => $staff["id"]);
			if ($mydb->insert('contact', $query)) {
				echo "Form submitted";
				$mydb->updateLeadCount($staff["id"]);
				echo "Email will be sent to ".$staff["FullName"]. " at ".$staff["Email"]; //Test email values correctly showing
				
				//Function to send email
				$mail->addAddress($staff["Email"], $staff["FullName"]);
				$mail->Subject = 'New Enquiry!';
				$mail->Body = 'Hi' .$staff["FullName"]. ',';
				$mail->Body .= '<p>A message has been made by <b>' .$_GET["fullname"]. '</b></p>';
				$mail->Body .= '<p>Please contact said person at - Tel: ' .$_GET["phone"]. ' or via email: ' .$_GET["email"]. '</p>';
				$mail->Body .= '<p>Address:<br />'.$_GET['address']. '<br />Lat: '.$_GET['latitude']. ' Longitude: ' .$_GET['longitude']. '</p>';
				if (!$mail->send()) {
					echo "Email not sent";
					echo "Error: ".$mail->ErrorInfo;
				}
				else {
					echo "Email sent!";
				}
			}
			else {
				echo "Failure";
			}
			break;
		default:
			# code...
			break;
	}
}
?>