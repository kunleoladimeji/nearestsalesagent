jQuery(document).ready(function($) {

	//TODO: Stricter validation for when user types in wrong address second time and onwards
	var validator = $("#contactForm").validate({
		ignore: [],
		rules: {
			fullname: "required",
			email: {
				required: true,
				email: true 
			},
			phone: {
				required: true,
				number: true
			},
			address: "required",
			message: {
				required: true,
				minlength: 50
			},
			hiddenRecaptcha: {
				required: function () {
					if (grecaptcha.getResponse()=='') {
						return true;
					} else {
						return false;
					}
				}
			}
		},
		messages: {
			fullname: "Please specify your full name",
			email: {
				required: "Please specify your email address",
				email: "Your email address must be in the format of name@domain.com"
			},
			phone: {
				required: "Please specify your phone number",
				number: "Please enter a valid number"
			},
			address: "Please specify your address",
			message: {
				required: "Please specify your message",
				minlength: "Message length cannot be less than 50 characters" 
			},
			hiddenRecaptcha: {
				required: "Please tick the captcha"
			}
		},
		submitHandler: function () {
			if ($("#staff").val()=='' || $("#staff").val()== null || parseInt($("#staff")) < 0) {
				BootstrapDialog.show({
					title: 'Contact Form',
					type: 'type-danger',
					message: 'We could not find that address, please try again',
					buttons: [{
						id: 'btn-address-clear',
						icon: 'glyphicon-wrench',
						label: 'Change address',
						cssClass: 'btn-primary',
						autospin: false,
						action: function(dialogRef) {
							dialogRef.close();
							$("#addressAutocomplete").focus();
						}
					}]
				});
			} else {
				BootstrapDialog.show({
				title: 'Contact Form',
				type: "type-info", 
    			message: 'Send Message?',
    			buttons: [{
			        id: 'btn-ok',   
			        icon: 'glyphicon glyphicon-check',       
			        label: 'Yes',
			        cssClass: 'btn-primary', 
			        autospin: true,
	        		action: function(dialogRef){
	        			$.ajax({
	        				url: 'inc/ajax.php',
	        				type: 'GET',
	        				data: {'action': 'submitForm', 'fullname': $('input#fullname').val(), 'email': $('input#email').val(), 'phone': $("input#phone").val(), 'address': $('input#addressAutocomplete').val(), 'message': $('textarea#message').val(), 'staff': $('input#staff').val(), 'longitude': $('#longitude').val(), 'latitude': $('#latitude').val() },
	        			})
	        			.done(function(data) {
	        				dialogRef.setType(BootstrapDialog.TYPE_SUCCESS);
	        				dialogRef.getButton('btn-ok').stopSpin();
	        				dialogRef.getButton('btn-ok').disable();
	        				dialogRef.getButton('btn-close').disable();
	        				dialogRef.getModalBody().html('Thanks for your message, our representative will in touch with you shortly<br /><br />This window will close automatically in 3 seconds'); 
		        			$('form').trigger('reset');
		        			grecaptcha.reset();
	        				console.log("success");
	        				setTimeout(function(){
                       			dialogRef.close();
                    			}, 3000);
	        			})
	        			.fail(function() {
	        				console.log("error");
	        				dialogRef.setType(BootstrapDialog.TYPE_DANGER);
	        				dialogRef.getModalBody().html('We are sorry, an error occured. Please contact us at <a href="mailto:kunleoladimeji@gmail.com">kunleoladimeji@gmail.com</a>'); 
	        				dialogRef.getButton('btn-ok').disable();
	        				dialogRef.getButton('btn-close').disable();
	        				setTimeout(function(){
                       			dialogRef.close();
                    			}, 1500);
	        			})
	        			.always(function() {
	        				console.log("complete");
	        			});
	        			
	        			}
	    			},
	    			{
	    				id: 'btn-close',
	    				icon: 'glyphicon glyphicon-eye-close',
	    				label: 'No',
	    				cssClass: 'btn-primary',
	    				autospin: false,
	    				action: function(dialogRef) {
	    					dialogRef.close();
	    				}
	    			}
	    			]
				});
			}
			
		}
	});
});
window.addressArray = [];
function geoLocate()
{
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
        });
    }
}

function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete((document.getElementById('addressAutocomplete')),{types: ['geocode'], componentRestrictions: {country: "fr"}});
        autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() {
	addressArray = [];
	 var place = autocomplete.getPlace();
	 $("#longitude").val(place.geometry.location.lat());
	 $("#latitude").val(place.geometry.location.lng())
	 $.get('inc/ajax.php', {'action': 'nearestStaff'}, function(data) {
	 	var size = Object.keys(data).length;
        for (var i = 1; i <= size; i++) {
            var address_formatted = data[i]["Address"];
            $.ajax({
            	url: 'https://maps.googleapis.com/maps/api/distancematrix/json',
            	type: 'GET',
            	data: {'units': 'imperial', 'origins' : $("#addressAutocomplete").val(), 'destinations' : address_formatted, 'mode' : 'driving', 'language' : 'en-GB', 'key' : 'AIzaSyCWiylvn-VZ5XnkSYQCAjRKC-h68_VbD1I'},
            })
            .done(function(data) {
            	var returnedCalculation = JSON.parse(jQuery(data["results"][0]).text());
                var distance = returnedCalculation.rows["0"].elements["0"].distance.value;
                addressArray.push(distance);
                $("#staff").val(addressArray.indexOf(Math.min(...addressArray)));
            })
            .fail(function() {
            	console.log("Could not get nearest staff!");
            })
            .always(function() {
            	console.log("Processing nearest staff");
            });    
        }
	 }, 'json');
}